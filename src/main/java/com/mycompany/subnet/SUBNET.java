/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.subnet;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author 66828
 */
public class SUBNET {

    public static void main(String[] args) {
        java.util.Scanner scanner = new java.util.Scanner(System.in);
        System.out.print("Enter IP address (e.g., 192.168.1.1): ");
        String ipAddress = scanner.next();
        System.out.print("Enter subnet prefix length (e.g., 24): ");
        int prefixLength = scanner.nextInt();

        try {
            InetAddress inetAddress = InetAddress.getByName(ipAddress);
            String networkClass = getNetworkClass(inetAddress.getAddress()[0]);
            String firstOctetRange = "";
            if ("C".equals(networkClass)) {
                firstOctetRange = "192-223";
            } else if ("B".equals(networkClass)) {
                firstOctetRange = "128-191";
            } else if ("A".equals(networkClass)) {
                firstOctetRange = "1-126";
            } else {
                firstOctetRange = "Unknown";
            }
            String hexIpAddress = bytesToHex(inetAddress.getAddress());
            String subnetMask = prefixLengthToSubnetMask(prefixLength);
            String wildcardMask = prefixLengthToWildcardMask(prefixLength);
            int subnetBits = prefixLength;
            int maskBits = 32 - prefixLength;
            int maximumSubnets = (int) Math.pow(2, maskBits);
            int hostsPerSubnet = (int) Math.pow(2, maskBits) - 2;
            String networkAddress = getNetworkAddress(inetAddress, prefixLength).getHostAddress();
            String broadcastAddress = getBroadcastAddress(inetAddress, prefixLength).getHostAddress();

            System.out.println("Network Class: " + networkClass);
            System.out.println("First Octet Range: " + firstOctetRange);
            System.out.println("Hex IP Address: " + hexIpAddress);
            System.out.println("IP Address: " + ipAddress);
            System.out.println("Subnet Mask: " + subnetMask);
            System.out.println("Wildcard Mask: " + wildcardMask);
            System.out.println("Subnet Bits: " + subnetBits);
            System.out.println("Mask Bits: " + maskBits);
            System.out.println("Maximum Subnets: " + maximumSubnets);
            System.out.println("Hosts per Subnet: " + hostsPerSubnet);
            System.out.println("Network Address: " + networkAddress);
            System.out.println("Broadcast Address: " + broadcastAddress);
        } catch (UnknownHostException e) {
            System.out.println("Invalid IP address or prefix length.");
        }
    }

    private static String getNetworkClass(byte firstOctet) {
        if ((firstOctet & 0xE0) == 0xC0) {
            return "C";
        } else if ((firstOctet & 0xC0) == 0x80) {
            return "B";
        } else if ((firstOctet & 0x80) == 0) {
            return "A";
        } else if ((firstOctet & 0xF0) == 0xE0) {
            return "D";
        } else {
            return "E";
        }
    }

    private static String getFirstOctetRange(byte firstOctet, int prefixLength) {
        int rangeEnd = (int) Math.pow(2, 8 - prefixLength) - 1;
        int start = (firstOctet & 0xFF) >> (8 - prefixLength) << (8 - prefixLength);
        int end = start + rangeEnd;
        return start + "-" + end;
    }

    private static String bytesToHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02X", b));
        }
        return sb.toString();
    }

    private static InetAddress getNetworkAddress(InetAddress inetAddress, int prefixLength) throws UnknownHostException {
        byte[] address = inetAddress.getAddress();
        for (int i = prefixLength; i < address.length * 8; i++) {
            address[i / 8] &= ~(1 << (7 - i % 8));
        }
        return InetAddress.getByAddress(address);
    }

    private static InetAddress getBroadcastAddress(InetAddress inetAddress, int prefixLength) throws UnknownHostException {
        byte[] address = inetAddress.getAddress();
        for (int i = prefixLength; i < address.length * 8; i++) {
            address[i / 8] |= (1 << (7 - i % 8));
        }
        return InetAddress.getByAddress(address);
    }

    private static String prefixLengthToSubnetMask(int prefixLength) {
        int mask = 0xFFFFFFFF << (32 - prefixLength);
        byte[] bytes = new byte[]{
            (byte) (mask >>> 24),
            (byte) (mask >> 16 & 0xFF),
            (byte) (mask >> 8 & 0xFF),
            (byte) (mask & 0xFF)
        };
        try {
            return InetAddress.getByAddress(bytes).getHostAddress();
        } catch (UnknownHostException e) {
            return null;
        }
    }

    private static String prefixLengthToWildcardMask(int prefixLength) {
        int mask = 0xFFFFFFFF << (32 - prefixLength);
        byte[] bytes = new byte[]{
            (byte) (~mask >>> 24),
            (byte) (~mask >> 16 & 0xFF),
            (byte) (~mask >> 8 & 0xFF),
            (byte) (~mask & 0xFF)
        };
        try {
            return InetAddress.getByAddress(bytes).getHostAddress();
        } catch (UnknownHostException e) {
            return null;
        }
    }

}
